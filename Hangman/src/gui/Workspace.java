package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import sun.font.TrueTypeFont;
import ui.AppGUI;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import data.GameData;


import java.io.IOException;

import static hangman.HangmanProperties.*;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import ui.loginDialogSingLeton;
import ui.creatProfileSingLeton;
import controller.HangmanController;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee,CHEN JIN
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    HangmanController controller;
    Label      guiHeadingLabel;   // workspace (GUI) heading label
    HBox       headPane;          // container to display the heading
    BorderPane       bodyPane;          // container for the main game displays
    ToolBar    footToolbar;       // toolbar for game buttons
//    BorderPane figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox       gameTextsPane;     // container to display the text-related parts of the game
    HBox       guessedLetters;    // text area displaying all the letters guessed so far
    HBox       remainingGuessBox; // container to display the number of remaining guesses
    Button     startGame;         // the button to start playing a game of Hangman
    Button     hint;
    VBox       hintPane;
    Canvas     canvas;
    GraphicsContext gc;
    ArrayList<TextField> textField;
    HBox       squarePane;
    ArrayList<TextField> abc;
    Button     CreateProfile;
    Button     Login;
    VBox       LeftChoice;
    GridPane   letterPane;
    Button     Button1;
    Button     Button2;
    Button     Button3;
    Button     Button4;
    Button     Button5;
    Button     Button6;
    Button     Button7;
    Button     Button8;
    Button     Button9;
    Button     Button10;
    Button     Button11;
    Button     Button12;
    Button     Button13;
    Button     Button14;
    Button     Button15;
    Button     Button16;
    Button     Help;
    ComboBox   comboBox;
    HBox       modeChoice;
    VBox       middlePane;
    int mode;
    int lv;
    Button name;
    Button Play;
    Button home;
    String Username;
    String Password;
    public GameData    gamedata;    // shared reference to the game being played, loaded or saved

    String level;
    public ScrollPane scrollPane = new ScrollPane();
    public Text helptext;
    HBox center;




    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    public void setStartGameDisable(){
        startGame.setDisable(true);
    }

    private void layoutGUI() {

        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));


        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

//        figurePane = new BorderPane();
        name = new Button("Chen");
        Play = new Button("Start Playing");
        home = new Button("Home");
        Help = new Button("Help");


        bodyPane = new BorderPane();
        bodyPane.setPrefSize(1000, 500);
        LeftChoice = new VBox();
        CreateProfile = new Button("Create new Profile");
        Login = new Button("Login");
        LeftChoice.getChildren().addAll(CreateProfile,Login,name,Play,home,Help);

        name.setVisible(false);
        Play.setVisible(false);
        home.setVisible(false);
        Help.setVisible(true);
        bodyPane.setLeft(LeftChoice);


        Button1 = new Button();
        Button1.setText("B");
        Button2 = new Button("U");
        Button3 = new Button("");
        Button4 = new Button();
        Button5 = new Button("Z");
        Button6 = new Button("Z");
        Button7 = new Button();
        Button8 = new Button();
        Button9 = new Button();
        Button10 = new Button();
        Button11 = new Button("W");
        Button12 = new Button("O");
        Button13 = new Button();
        Button14 = new Button();
        Button15 = new Button("R");
        Button16 = new Button("D");

        Button1.setPrefSize(40,40);
        Button2.setPrefSize(40,40);
        Button3.setPrefSize(40,40);
        Button4.setPrefSize(40,40);
        Button5.setPrefSize(40,40);
        Button6.setPrefSize(40,40);
        Button7.setPrefSize(40,40);
        Button8.setPrefSize(40,40);
        Button9.setPrefSize(40,40);
        Button10.setPrefSize(40,40);
        Button11.setPrefSize(40,40);
        Button12.setPrefSize(40,40);
        Button13.setPrefSize(40,40);
        Button14.setPrefSize(40,40);
        Button15.setPrefSize(40,40);
        Button16.setPrefSize(40,40);




        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);
        letterPane.add(Button1,0,0);
        letterPane.add(Button2,1,0);
        letterPane.add(Button3,2,0);
        letterPane.add(Button4,3,0);
        letterPane.add(Button5,0,1);
        letterPane.add(Button6,1,1);
        letterPane.add(Button7,2,1);
        letterPane.add(Button8,3,1);
        letterPane.add(Button9,0,2);
        letterPane.add(Button10,1,2);
        letterPane.add(Button11,2,2);
        letterPane.add(Button12,3,2);
        letterPane.add(Button13,0,3);
        letterPane.add(Button14,1,3);
        letterPane.add(Button15,2,3);
        letterPane.add(Button16,3,3);


        HBox blankBoxLeft2  = new HBox();
        HBox blankBoxRight2 = new HBox();
        HBox.setHgrow(blankBoxLeft2, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight2, Priority.ALWAYS);
        center = new HBox();
        center.getChildren().addAll(blankBoxLeft2,letterPane,blankBoxRight2);
        bodyPane.setCenter(center);




        startGame = new Button("BUZZWORD");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }



    private void setupHandlers(){
        helptext = new Text("An Introduction to Buzzword\n" +
                "\n" +
                "Buzzword is a word search game in which players try to find as many words as possible using adjacent letters in a limited time. Designed by Allan Turoff and manufactured by Parker Brothers, a division of the Hasbro Company, this game has been around a while and is probably the forerunner to such games as Word Whomp and Text Twist.\n" +
                "\n" +
                "This game can be played by any number of players although the number may be limited by the ability to see the letters in the boggle tray. In most cases, this game can easily be played by 2 to 8 people.\n" +
                "\n" +
                "Object of The Game\n" +
                "\n" +
                "The object of the game is to see how many points you can accumulate in a three-minute period by finding the most or longest words that are different from the words that everyone else has found.\n" +
                "\n" +
                "Skills Needed To Play\n" +
                "\n" +
                "While Boggle isn't a difficult game to play, there are some skills that are necessary to be able to play this game successfully. These skills include a reasonable vocabulary, word recognition skills and spelling. For this reason, this game is best suited for ages 10 and up although children as young as 8 have been quite successful at playing this word game. There is also a version of this game called Boggle junior designed specifically for younger children.\n" +
                "\n" +
                "Educational Value Of This Game For Children\n" +
                "\n" +
                "Playing Boggle with your children has a huge educational value for them. It will improve their reading and word recognition skills as well as their spelling. Since every subject in school is based on the ability to read, this game may help your child to do better at every subject across the board. You can easily base this game on beginning skills to allow children who are just beginning to read to use two and three letter words to help increase their reading and word recognition skills.\n" +
                "\n" +
                "Where To Get or Play The Game\n" +
                "\n" +
                "You can purchase the board type version of this game, play Boggle or boggle clones online or download the game to play on your computer.\n" +
                "\n" +
                "The Boggle Board Game\n" +
                "\n" +
                "The boggle board game comes with the letter cubes, the plastic holder and a small timer. All players will also need a pen or pencil and paper to play the game.\n" +
                "\n" +
                "How To Play Boggle (The Original)\n" +
                "\n" +
                "The boggle game has 16 cubes with each cube having a different letter printed on each side. There is a plastic tray with a lid that allows you to shake the dice inside and then when the dice settles you remove the lid and start the 3-minute timer (egg timer that comes with the game)\n" +
                "\n" +
                "1. As soon as the timer is set, all players start making as many words as possible in the three minute time period.\n" +
                "\n" +
                "2. All words must be at least 3 letters long and all letters used to make a word must be adjacent to one another either vertically, horizontally or diagonally. A letter can only be used one time in the making of a word.\n" +
                "\n" +
                "3. When the timer runs out, play stops and each player takes turns reading his words from the list he has made.\n" +
                "\n" +
                "4. If two or more players have the same word, the word is crossed off and cannot be counted in the point count.\n" +
                "\n" +
                "5. After all the players have read their words, the scores are counted and the person with the most points wins.\n" +
                "\n" +
                "6. Longer words get more points. 3-4 letter words score 1 point, 5 letter words score 2 points, 6 letter words score 3 points, 7 letter words score 5 points and 8 letter words score 11 points.\n" +
                "\n" +
                "A new game then can be started or you can decide before playing to play a certain number of rounds and then add up the total of all the rounds to declare a winner at the end of the game.\n" +
                "\n" +
                "Basic Strategies For Improving Your game\n" +
                "\n" +
                "Let's be honest everyone loves winning or at least be close to the winning score and there are some strategies that can help you score more points when playing this game.\n" +
                "\n" +
                "Look for plurals, and words with \"ed\" or \"er\" and \"ing\" at the end. This may help you score more points per word.\n" +
                "When you make a long word, check to see if you can also split the word into smaller words. For example, the word bearing will also allow you to make the words \"bear\" and \"ring\" using the same letters to make three words.\n" +
                "Think backwards. Many times people see obvious words such as \"star\" or \"deer\" and miss the fact that writing the letters backwards also creates another word in this case \"rats\" and \"reed\".\n" +
                "Look for less common word among letters. For example most people will easily find the word \"rode\" but, less will see that these letters also spell \"doer\".\n" +
                "By following these simple strategies over time, you should be able to increase those Boogle scores by several points.\n" +
                "\n" +
                "Boggle Tournaments\n" +
                "\n" +
                "While Boggle tournaments are not as popular as Scrabble tournaments they do exist and can be a lot of fun. So if you are really good at this game you might want to consider looking for a tournament in your area or at least starting a Saturday night Boggle party for those who love the game");
        controller =  (HangmanController) gui.getFileComponent();

        Login.setOnAction(event -> {
            loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();
            dialog.show(1);
            try {
                controller.handleLoginRequest();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Help.setOnAction(event -> {

            scrollPane.setContent(helptext);
            bodyPane.setCenter(scrollPane);
            Help.setOnAction(event1 -> {

                bodyPane.setCenter(center);


            });

        });

        CreateProfile.setOnAction(event -> {
            gamedata = new GameData(app);


            creatProfileSingLeton dialog = creatProfileSingLeton.getSingleton();
            dialog.show(1);
            Username=dialog.getuserName();
            Password= dialog.getPassWord();
//            System.out.println(gamedata.getuserName());

            gamedata.setuserName(Username);
            gamedata.setpassWord(Password);
            gamedata.setLevel("1");


            controller.handlecreatNewRequest();
        });

    }

    public void goBackHome(){
        name.setVisible(true);
        Play.setVisible(true);
        comboBox.setVisible(true);
        CreateProfile.setVisible(false);
        Login.setVisible(false);
        Help.setVisible(false);
        home.setVisible(false);




        Button1 = new Button();
        Button1.setText("B");
//        Button1.setShape(new Circle(0.5));
        Button2 = new Button("U");
        Button3 = new Button("");
        Button4 = new Button();
        Button5 = new Button("Z");
        Button6 = new Button("Z");
        Button7 = new Button();
        Button8 = new Button();
        Button9 = new Button();
        Button10 = new Button();
        Button11 = new Button("W");
        Button12 = new Button("O");
        Button13 = new Button();
        Button14 = new Button();
        Button15 = new Button("R");
        Button16 = new Button("D");

        Button1.setPrefSize(40,40);
        Button2.setPrefSize(40,40);
        Button3.setPrefSize(40,40);
        Button4.setPrefSize(40,40);
        Button5.setPrefSize(40,40);
        Button6.setPrefSize(40,40);
        Button7.setPrefSize(40,40);
        Button8.setPrefSize(40,40);
        Button9.setPrefSize(40,40);
        Button10.setPrefSize(40,40);
        Button11.setPrefSize(40,40);
        Button12.setPrefSize(40,40);
        Button13.setPrefSize(40,40);
        Button14.setPrefSize(40,40);
        Button15.setPrefSize(40,40);
        Button16.setPrefSize(40,40);

        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);
        letterPane.add(Button1,0,0);
        letterPane.add(Button2,1,0);
        letterPane.add(Button3,2,0);
        letterPane.add(Button4,3,0);
        letterPane.add(Button5,0,1);
        letterPane.add(Button6,1,1);
        letterPane.add(Button7,2,1);
        letterPane.add(Button8,3,1);
        letterPane.add(Button9,0,2);
        letterPane.add(Button10,1,2);
        letterPane.add(Button11,2,2);
        letterPane.add(Button12,3,2);
        letterPane.add(Button13,0,3);
        letterPane.add(Button14,1,3);
        letterPane.add(Button15,2,3);
        letterPane.add(Button16,3,3);



        HBox blankBoxLeft2  = new HBox();
        HBox blankBoxRight2 = new HBox();
        HBox.setHgrow(blankBoxLeft2, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight2, Priority.ALWAYS);
        HBox center = new HBox();
        center.getChildren().addAll(blankBoxLeft2,letterPane,blankBoxRight2);
        bodyPane.setCenter(center);
        bodyPane.setRight(null);




        startGame = new Button("BUZZWORD");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

    }



    public void logoutUser(){
        name.setVisible(false);
        Play.setVisible(false);
        comboBox.setVisible(false);
        CreateProfile.setVisible(true);
        Login.setVisible(true);
        Help.setVisible(true);
        home.setVisible(false);




        Button1 = new Button();
        Button1.setText("B");
//        Button1.setShape(new Circle(0.5));
        Button2 = new Button("U");
        Button3 = new Button("");
        Button4 = new Button();
        Button5 = new Button("Z");
        Button6 = new Button("Z");
        Button7 = new Button();
        Button8 = new Button();
        Button9 = new Button();
        Button10 = new Button();
        Button11 = new Button("W");
        Button12 = new Button("O");
        Button13 = new Button();
        Button14 = new Button();
        Button15 = new Button("R");
        Button16 = new Button("D");

        Button1.setPrefSize(40,40);
        Button2.setPrefSize(40,40);
        Button3.setPrefSize(40,40);
        Button4.setPrefSize(40,40);
        Button5.setPrefSize(40,40);
        Button6.setPrefSize(40,40);
        Button7.setPrefSize(40,40);
        Button8.setPrefSize(40,40);
        Button9.setPrefSize(40,40);
        Button10.setPrefSize(40,40);
        Button11.setPrefSize(40,40);
        Button12.setPrefSize(40,40);
        Button13.setPrefSize(40,40);
        Button14.setPrefSize(40,40);
        Button15.setPrefSize(40,40);
        Button16.setPrefSize(40,40);

        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);
        letterPane.add(Button1,0,0);
        letterPane.add(Button2,1,0);
        letterPane.add(Button3,2,0);
        letterPane.add(Button4,3,0);
        letterPane.add(Button5,0,1);
        letterPane.add(Button6,1,1);
        letterPane.add(Button7,2,1);
        letterPane.add(Button8,3,1);
        letterPane.add(Button9,0,2);
        letterPane.add(Button10,1,2);
        letterPane.add(Button11,2,2);
        letterPane.add(Button12,3,2);
        letterPane.add(Button13,0,3);
        letterPane.add(Button14,1,3);
        letterPane.add(Button15,2,3);
        letterPane.add(Button16,3,3);



        HBox blankBoxLeft2  = new HBox();
        HBox blankBoxRight2 = new HBox();
        HBox.setHgrow(blankBoxLeft2, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight2, Priority.ALWAYS);
        HBox center = new HBox();
        center.getChildren().addAll(blankBoxLeft2,letterPane,blankBoxRight2);
        bodyPane.setCenter(center);
        bodyPane.setRight(null);




        startGame = new Button("BUZZWORD");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);


    }

    public void levelSelection(){

        CreateProfile.setVisible(false);
        name.setVisible(true);
        Play.setVisible(true);

//        appTemplate.getWorkspaceComponent()
        //System.out.println(LeftChoice.getChildren().get(0).toString());
        comboBox = new ComboBox();
        comboBox.getItems().addAll(
                "English Dictionary",
                "Places",
                "Animal"
        );

        comboBox.setPrefSize(130,50);
        comboBox.setPromptText("Select Mode");
        Login.setVisible(false);
        Help.setVisible(false);
        LeftChoice.getChildren().addAll(comboBox);
        bodyPane.setLeft(LeftChoice);
        Play.setOnAction(e ->{
            if(comboBox.getValue().toString() == "English Dictionary"){
//            System.out.println("good");
                SelectionPage(1);
            }
            if(comboBox.getValue().toString() == "Places"){
//                System.out.println("science");
                SelectionPage(2);

            }
            if(comboBox.getValue().toString() == "Animal"){
//                System.out.println("Animal");
                SelectionPage(3);
            }
        }

        );


        name.setOnAction(event -> {
            logoutUser();
        });


    }




    public int getMode(){return mode;}
    public void SelectionPage(int mode6) {

        Play.setVisible(false);
        mode = mode6;
        Login.setVisible(false);
        Help.setVisible(false);
        home.setVisible(true);

        home.setOnAction(event -> {
            goBackHome();
        });

        comboBox.setVisible(false);
        HBox name = new HBox();
        name.getChildren().addAll(new Label("Places"));
        Button1 = new Button("1");
        Button2 = new Button("2");
        Button3 = new Button("3");
        Button4 = new Button("4");

        Button1.setPrefSize(40,40);
        Button2.setPrefSize(40,40);
        Button3.setPrefSize(40,40);
        Button4.setPrefSize(40,40);


        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);
        letterPane.add(Button1,0,0);
        letterPane.add(Button2,1,0);
        letterPane.add(Button3,2,0);
        letterPane.add(Button4,3,0);

        Button2.setDisable(true);
        Button3.setDisable(true);
        Button4.setDisable(true);


        if(controller.getLevelForWordspace()!=null) {
            if (controller.getLevelForWordspace().equals("1")) {
                Button2.setDisable(true);
                Button3.setDisable(true);
                Button4.setDisable(true);

            }
            if (controller.getLevelForWordspace().equals("2")) {
                Button2.setDisable(false);
                Button3.setDisable(true);
                Button4.setDisable(true);

            }
            if (controller.getLevelForWordspace().equals("3")) {
                Button2.setDisable(false);
                Button3.setDisable(false);
                Button4.setDisable(true);

            }
            if (controller.getLevelForWordspace().equals("4")) {
                Button2.setDisable(false);
                Button3.setDisable(false);
                Button4.setDisable(false);


            }
        }



        middlePane = new VBox();
        modeChoice = new HBox();
        if (mode == 1) {
            modeChoice.getChildren().addAll(new Label("English Dictionary"));
        }
        if (mode == 2){
            modeChoice.getChildren().addAll(new Label("Places"));
        }
        if (mode == 3){
            modeChoice.getChildren().addAll(new Label("Animal"));
        }


        HangmanController con= new HangmanController(app, startGame);
        Button1.setOnAction(e ->{
            lv=1;
            con.start();
        });
        Button2.setOnAction(e ->{
            lv=2;
            con.start();
        });
        Button3.setOnAction(e ->{
            lv=3;
            con.start();
        });
        Button4.setOnAction(e ->{
            lv=4;
            con.start();
        });

        middlePane.getChildren().addAll(modeChoice,letterPane);

        HBox blankBoxLeft2  = new HBox();
        HBox blankBoxRight2 = new HBox();
        HBox.setHgrow(blankBoxLeft2, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight2, Priority.ALWAYS);
        HBox center = new HBox();
        center.getChildren().addAll(blankBoxLeft2,middlePane,blankBoxRight2);
        bodyPane.setCenter(center);


    }

    public void handleLogout() {
        
    }
    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {

    }

    public Button getName(){return name;}

    public int      getLv(){return lv;}
    public GridPane getletterPane(){return letterPane;}

    public HBox getmodeChoice(){return modeChoice;}
    public VBox getMiddlePane(){return middlePane;}
    public void setMiddlePane(VBox middlePane){this.middlePane=middlePane;}

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public HBox getsquarePane(){return squarePane;}

    public BorderPane getBodyPane(){return bodyPane; }

    public Button getStartGame() {
        return startGame;
    }

    public Button getHint(){return hint;}

    public Button getCreateProfile(){return CreateProfile;}

    public Button getPlay(){return Play;}


    public void reinitialize() {

        bodyPane = new BorderPane();

        LeftChoice = new VBox();
        letterPane = new GridPane();

        bodyPane.setLeft(LeftChoice);
        bodyPane.setCenter(letterPane);


    }
}
