 package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import hangman.Hangman;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.CharacterCodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import javafx.scene.layout.VBox;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

import java.util.ArrayList;


import javafx.scene.canvas.Canvas;


import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import ui.AppGUI;
import javafx.geometry.Insets;
import javafx.scene.Scene;

import java.io.IOException;

import static hangman.HangmanProperties.*;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import java.util.ArrayList;
import java.util.stream.Stream;

import ui.loginDialogSingLeton;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.scene.input.KeyEvent;


 /**
 * @author Ritwik Banerjee, CHEN JIN
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private Workspace   gameWorkspace;
    public  static GameData   gamedata;    // shared reference to the game being played, loaded or saved
    public Text      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private Path        workFile;
    private Button      hintButton;
    private Label       Guessed;
    public  HBox        Level;
    public  HBox        timer;
    public  HBox        word;
    public  VBox        Words;
    public  VBox        Target;
    public Button       pause;
    public Button       replay;
    public HBox         PauseBox;
    public VBox         middlePane;
    public GridPane      letterPane;
    public TableView table = new TableView();
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;
    private static final int TOTAL_NUMBER_OF_ANIMALS_WORDS    = 39;
    public String targetword;
    public Label timerLabel = new Label();
    private static final Integer STARTTIME = 15;
    private IntegerProperty timeSeconds =
            new SimpleIntegerProperty(STARTTIME);
    public Timeline timeline;
    public Integer pauseFlag = 0;
    private final static int SIZE = 4;
    private char[][] board;
    private Button[][] tdbotton;
    TextField tx;
    public String guessedLetter = new String();

    public ArrayList<String> exitWord  = new ArrayList<String>();
     ;
     ObservableList<String> items;
     String search = new String();
     ListView<String> list;
     public int singlePoint;
     public int totalPoints;
     public Label totalPointsLabel;
     public char guess;
     public String dragword;
     public ArrayList<String> dictionary;
    Button     Button1;
    Button     Button2;
    Button     Button3;
    Button     Button4;
    Button     Button5;
    Button     Button6;
    Button     Button7;
    Button     Button8;
    Button     Button9;
    Button     Button10;
    Button     Button11;
    Button     Button12;
    Button     Button13;
    Button     Button14;
    Button     Button15;
    Button     Button16;

    String     LoginUserName;

    String     levelForWordspace;


    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;

    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
//        gameButton.setDisable(false);
    }

    public void disableGameButton(){
        gameButton.setDisable(true);
    }


    public void start() {

        gamedata = (GameData)appTemplate.getDataComponent();
        System.out.println(gamedata.getLevel());


        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();


        BorderPane bodyPane = gameWorkspace.getBodyPane();
//        middlePane = gameWorkspace.getMiddlePane();
        HBox       modeChoice = gameWorkspace.getmodeChoice();
//        GridPane   letterPane =gameWorkspace.getletterPane();
        middlePane = new VBox();
        int lv = gameWorkspace.getLv();

        bodyPane.setCenter(null);

        pause    = new Button("play");
        pause.setPrefSize(100,40);

        replay = new Button("REPLAY");
        replay.setPrefSize(100,40);

        Random r = new Random();
        int a = r.nextInt(2);


        BoggleBoard();

        dictionary = new ArrayList<>();
        dictionary = getdictionary();
        for(int i = 0;i<dictionary.size();i++) {
            targetword = dictionary.get(i);
            findWord(targetword);
        }



        int mode = gameWorkspace.getMode();
        modeChoice = new HBox();
        if (mode == 1) {
            modeChoice.getChildren().addAll(new Label("English Dictionary"));
        }
        if (mode == 2){
            modeChoice.getChildren().addAll(new Label("Places"));
        }
        if (mode == 3){
            modeChoice.getChildren().addAll(new Label("Animal"));
        }

        letterPane.setVisible(false);


        Level = new HBox();
        if (lv == 1) {
            Level.getChildren().addAll(new Label("level 1"));
        }
        if (lv == 2){
            Level.getChildren().addAll(new Label("level 2"));
        }
        if (lv == 3){
            Level.getChildren().addAll(new Label("level 3"));
        }
        if (lv == 4){
            Level.getChildren().addAll(new Label("level 4"));
        }

        middlePane.getChildren().addAll(modeChoice,letterPane,Level);

        PauseBox = new HBox();
        PauseBox.getChildren().addAll(pause,replay);

        middlePane.getChildren().addAll(PauseBox);

        HBox blankBoxLeft2  = new HBox();
        HBox blankBoxRight2 = new HBox();
        HBox.setHgrow(blankBoxLeft2, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight2, Priority.ALWAYS);
        HBox center = new HBox();
        center.getChildren().addAll(blankBoxLeft2,middlePane,blankBoxRight2);
        bodyPane.setCenter(center);


        System.out.println(exitWord);


        replay.setOnAction(event -> {
//            replayGame();
        });

        pause.setOnAction(new EventHandler<ActionEvent>(){

            public void handle(ActionEvent event ) {
            if (timeline != null) {
                timeline.stop();
            }
                setpause();

                if (pauseFlag == 0){//start on the round
                    timeSeconds.set(STARTTIME);
                    timeline = new Timeline();
                    timeline.getKeyFrames().add(
                            new KeyFrame(Duration.seconds(STARTTIME+1),
                                    new KeyValue(timeSeconds, 0)));
                    timeline.playFromStart();
                    pauseFlag = 1; // in the round
                } else if(pauseFlag == 1) {
                    timeline.stop();
                    pauseFlag = 2;
                }else if (pauseFlag == 2){
                    timeline.play();
                    pauseFlag = 1;
                }


                timeline.setOnFinished(new EventHandler<ActionEvent>(){

                    @Override
                    public void handle(ActionEvent arg0) {
//                        timeline.play();
                        System.out.println("finished");
                        pause.setText("play");
                        for (int a =0;a<exitWord.size();a++){
                            items.add(exitWord.get(a));
                        }
                    }

                });


        }
        });

        timerLabel.setText(timeSeconds.toString());
        timerLabel.setTextFill(Color.RED);
        timerLabel.textProperty().bind(timeSeconds.asString());


        items =FXCollections.observableArrayList ();

        dragword ="";
        handleMouseDrag();

//        items =FXCollections.observableArrayList ();

        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {

            if(pause.getText().equals("play")){}
            else {
                guess = event.getCharacter().charAt(0);
                if ((guess >= 65 && guess <= 90) || (guess >= 97 && guess <= 122)) {
                    guessedLetter = progress.getText();
                    guessedLetter += guess;
                    updateButtonColor();
//                System.out.println(guessedLetter);

                    progress.setText(guessedLetter);
                }
                if (guess == 13) {
                    clearButtonColor();

                    list.setItems(items);
                    guessedLetter = progress.getText();

                    for (int i = 0; i < exitWord.size(); i++) {
                        if (guessedLetter.equals(exitWord.get(i))) {
                            singlePoint = (exitWord.get(i).length() - 2) * 10;
                            totalPoints = totalPoints + singlePoint;
                            System.out.println(totalPoints);
                            totalPointsLabel.setText(Integer.toString(totalPoints));

                            progress.setText("");
                            items.add(guessedLetter + "     " + singlePoint);

                        }

                    }

                    progress.setText("");
                }
            }
        });


        timer = new HBox();
        timer.getChildren().addAll(new Label("TIME REMAINING: "),timerLabel);


        word = new HBox();

        progress = new Text();

        word.getChildren().addAll(progress);

        items =FXCollections.observableArrayList ();
        list = new ListView<String>();
        list.setItems(items);



        Target = new VBox();
        System.out.println(totalPoints);
        totalPointsLabel = new Label();
        totalPointsLabel.setText(Integer.toString(totalPoints));

        Target.getChildren().addAll(new Label("Total Points:      " ),totalPointsLabel,new Label("Target Points:    "+Integer.toString(lv*10)));



        VBox right = new VBox();
        right.getChildren().addAll(timer,word,list,Target);
        bodyPane.setRight(right);











//        play();
    }

    public void replayGame(){
        start();
    }

     public void checkDragWordStatus(){
        for (int i = 0; i < exitWord.size(); i++) {
        if (dragword.equals(exitWord.get(i))) {
            singlePoint = (exitWord.get(i).length() - 2) * 10;
            totalPoints = totalPoints + singlePoint;
            totalPointsLabel.setText(Integer.toString(totalPoints));
            items.add(dragword + "     " + singlePoint);
            dragword = "";
            clearButtonColor();

        }
        if(dragword.length()>5){
            clearButtonColor();
            dragword = "";


        }

        }

    }
    public void handleMouseDrag() {

         tdbotton[0][0].setOnDragDetected(event ->{
//             System.out.println("button1" );
             tdbotton[0][0].setStyle("-fx-base: lightgreen;");
             dragword = dragword + tdbotton[0][0].getText();
             checkDragWordStatus();

         });
         tdbotton[1][0].setOnDragDetected(event ->{
//             System.out.println("button2" );
             tdbotton[1][0].setStyle("-fx-base: lightgreen;");
             dragword = dragword + tdbotton[1][0].getText();
             checkDragWordStatus();

         });
        tdbotton[2][0].setOnDragDetected(event ->{
//            System.out.println("button3" );
            tdbotton[2][0].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[2][0].getText();
            checkDragWordStatus();


        });
        tdbotton[3][0].setOnDragDetected(event ->{
//            System.out.println("button4" );
            tdbotton[3][0].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[3][0].getText();
            checkDragWordStatus();


        });
         tdbotton[0][1].setOnDragDetected(event ->{
//             System.out.println("button5" );
             tdbotton[0][1].setStyle("-fx-base: lightgreen;");
             dragword = dragword + tdbotton[0][1].getText();
             checkDragWordStatus();


         });
        tdbotton[1][1].setOnDragDetected(event ->{
//            System.out.println("button6" );
            tdbotton[1][1].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[1][1].getText();
            checkDragWordStatus();


        });
        tdbotton[2][1].setOnDragDetected(event ->{
//            System.out.println("button7" );
            tdbotton[2][1].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[2][1].getText();
            checkDragWordStatus();


        });
        tdbotton[3][1].setOnDragDetected(event ->{
//            System.out.println("button8" );
            tdbotton[3][1].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[3][1].getText();
            checkDragWordStatus();


        });
        tdbotton[0][2].setOnDragDetected(event ->{
//            System.out.println("button9" );
            tdbotton[0][2].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[0][2].getText();
            checkDragWordStatus();


        });
        tdbotton[1][2].setOnDragDetected(event ->{
//            System.out.println("button10" );
            tdbotton[1][2].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[1][2].getText();
            checkDragWordStatus();


        });
        tdbotton[2][2].setOnDragDetected(event ->{
//            System.out.println("button11" );
            tdbotton[2][2].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[2][2].getText();
            checkDragWordStatus();


        });
        tdbotton[3][2].setOnDragDetected(event ->{
//            System.out.println("button12" );
            tdbotton[3][2].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[3][2].getText();
            checkDragWordStatus();


        });
        tdbotton[0][3].setOnDragDetected(event ->{
//            System.out.println("button13" );
            tdbotton[0][3].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[0][3].getText();
            checkDragWordStatus();


        });
        tdbotton[1][3].setOnDragDetected(event ->{
//            System.out.println("button14" );
            tdbotton[1][3].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[1][3].getText();
            checkDragWordStatus();


        });
        tdbotton[2][3].setOnDragDetected(event ->{
//            System.out.println("button15" );
            tdbotton[2][3].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[2][3].getText();
            checkDragWordStatus();


        });
        tdbotton[3][3].setOnDragDetected(event -> {
//            System.out.println("button16");
            tdbotton[3][3].setStyle("-fx-base: lightgreen;");
            dragword = dragword + tdbotton[3][3].getText();
            checkDragWordStatus();


        });



    }

    public void updateButtonColor(){
        int a;
        for (int r = 0; r < this.board.length; r++) {
            for (int c = 0; c < this.board.length; c++) {
                a = guessedLetter.length();
                if (tdbotton[r][c].getText().equals(guessedLetter.substring(a-1,a))){
                    tdbotton[r][c].setStyle("-fx-base: lightgreen;");
                    if(r-1 > 0 && c-1>0){
                        if (tdbotton[r-1][c-1].getText().equals(guessedLetter.substring(a))){

                        }
                    }
                    if(r+1>= this.board.length){

                    }
                }
            }
        }
    }

    public void clearButtonColor(){
        for (int r = 0; r < this.board.length; r++) {
            for (int c = 0; c < this.board.length; c++) {
                tdbotton[r][c].setStyle("-fx-base: white;");

             }
        }
    }


    public void BoggleBoard() {

        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);

        this.tdbotton = new Button[SIZE][SIZE];
        this.board = new char[SIZE][SIZE];
        for (int r = 0; r < this.board.length; r++) {
            for (int c = 0; c < this.board.length; c++) {
                this.board[r][c] = (char)('a' + (int)(Math.random()*26));

                tdbotton[r][c] = new Button();
                tdbotton[r][c].setPrefSize(40,40);
                tdbotton[r][c].setText(Character.toString(this.board[r][c]));
                tdbotton[r][c].setStyle("-fx-base: white;");

                letterPane.add(tdbotton[r][c],r,c);

            }
        }

    }

    public boolean findWord(String word) {
        for (int row = 0; row < this.board.length; row++) {
            for (int col = 0; col < this.board.length; col++) {
                if (this.findWord(word, row, col)) {
                    exitWord.add(word);
                    return true;
                }

            }
        }
        return false;
    }

    private boolean findWord(String word, int row, int col) {
        if (word.equals("")) {
            return true;
        }
        else if (row < 0 || row >= this.board.length ||
                col < 0 || col >= this.board.length ||
                this.board[row][col] != word.charAt(0)) {
            return false;
        }
        else {
            char safe = this.board[row][col];
            this.board[row][col] = '*';
            String rest = word.substring(1, word.length());
            boolean result = this.findWord(rest, row-1, col-1) ||
                    this.findWord(rest, row-1,   col) ||
                    this.findWord(rest, row-1, col+1) ||
                    this.findWord(rest,   row, col-1) ||
                    this.findWord(rest,   row, col+1) ||
                    this.findWord(rest, row+1, col-1) ||
                    this.findWord(rest, row+1,   col) ||
                    this.findWord(rest, row+1, col+1);
            this.board[row][col] = safe;
            if (result == true){
            }

            return result;



        }
    }

    public String toString() {
        String str = "";
        for (int r = 0; r < board.length; r++) {
            for (char ch : board[r]) {
                str += ch + " ";
            }
            if (r < board.length-1) {
                str += "\n";
            }
        }
        return str;
    }


    public void generateLetterPane(){
        Button1 = new Button();
        Button2 = new Button();
        Button3 = new Button();
        Button4 = new Button();
        Button5 = new Button();
        Button6 = new Button();
        Button7 = new Button();
        Button8 = new Button();
        Button9 = new Button();
        Button10 = new Button();
        Button11 = new Button();
        Button12 = new Button();
        Button13 = new Button();
        Button14 = new Button();
        Button15 = new Button();
        Button16 = new Button();
        Button1.setPrefSize(40,40);
        Button2.setPrefSize(40,40);
        Button3.setPrefSize(40,40);
        Button4.setPrefSize(40,40);
        Button5.setPrefSize(40,40);
        Button6.setPrefSize(40,40);
        Button7.setPrefSize(40,40);
        Button8.setPrefSize(40,40);
        Button9.setPrefSize(40,40);
        Button10.setPrefSize(40,40);
        Button11.setPrefSize(40,40);
        Button12.setPrefSize(40,40);
        Button13.setPrefSize(40,40);
        Button14.setPrefSize(40,40);
        Button15.setPrefSize(40,40);
        Button16.setPrefSize(40,40);

        char[] letters = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        Button1.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button2.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button3.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button4.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button5.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button6.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button7.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button8.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button9.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button10.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button11.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button12.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button13.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button14.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button15.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());
        Button16.setText(Character.toString(letters[(int)(Math.random()*26)]).toUpperCase());



        letterPane = new GridPane();
        letterPane.setVgap(20);
        letterPane.setHgap(20);
        letterPane.add(Button1,0,0);
        letterPane.add(Button2,1,0);
        letterPane.add(Button3,2,0);
        letterPane.add(Button4,3,0);
        letterPane.add(Button5,0,1);
        letterPane.add(Button6,1,1);
        letterPane.add(Button7,2,1);
        letterPane.add(Button8,3,1);
        letterPane.add(Button9,0,2);
        letterPane.add(Button10,1,2);
        letterPane.add(Button11,2,2);
        letterPane.add(Button12,3,2);
        letterPane.add(Button13,0,3);
        letterPane.add(Button14,1,3);
        letterPane.add(Button15,2,3);
        letterPane.add(Button16,3,3);
    }

    public void setpause(){
        if(pause.getText().equals("pause")){
            letterPane.setVisible(false);
            pause.setText("play");
            guessedLetter = null;

        }

        else{
            letterPane.setVisible(true);
            pause.setText("pause");
        }


    }

//    public void setempty(){
//        Button1.setText("");
//        Button2.setText("");
//        Button3.setText("");
//        Button4.setText("");
//        Button5.setText("");
//        Button6.setText("");
//        Button7.setText("");
//        Button8.setText("");
//        Button9.setText("");
//        Button10.setText("");
//        Button11.setText("");
//        Button12.setText("");
//        Button13.setText("");
//        Button14.setText("");
//        Button15.setText("");
//        Button16.setText("");
//    }

    public String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;
        boolean nonLetter = true;
        String targetWord ="";

//        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            while(nonLetter){
                nonLetter = false;
                int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
                targetWord =lines.skip(toSkip).findFirst().get();
                for (char a: targetWord.toCharArray()){
                    if(!Character.isLetter(a)){
                        nonLetter = true;
                    }
                }
            }
            return targetWord;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }


    public ArrayList<String> getdictionary(){
        ArrayList<String> dictionary = new ArrayList<>();
        String word;
        try {

            FileReader fr = new FileReader("/Users/mac/Desktop/BuzzWord/out/production/Hangman/words/1.txt");

            BufferedReader br = new BufferedReader(fr);

            while((word = br.readLine())!=null){
                if (word.length()>=3){
                    dictionary.add(word.split(":")[0]);
                }
            }

            return dictionary;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");

    }

    public void setmiddlepane(){
//        middlePane().setVisible(false);
    }

    private void end() {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();


        messageDialog.show("Game end",success ? "You win!" : "Ah, close but not quite there.");
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        if(hintButton!= null)
            hintButton.setVisible(false);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);



    }

//    private void initWordGraphics(HBox guessedLetters) {
//
//        char[] targetword = gamedata.getTargetWord().toCharArray();
//
//        progress = new Text[targetword.length];
//        if(progress.length>=7){
//            enableHintButton();
//        }
//        for (int i = 0; i < progress.length; i++) {
//            progress[i] = new Text(Character.toString(targetword[i]));
//            progress[i].setVisible(false);
//        }
//        guessedLetters.getChildren().addAll(progress);
//
//        Guessed = new Label();
//
//
//
//
//    }


    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

            @Override
            public void handle(long now) {



                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);

                    if (!Character.toString(guess).matches("[a-z]")) {
                        PropertyManager propertyManager = PropertyManager.getManager();
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

                        dialog.show(propertyManager.getPropertyValue(New_INPUT_ERROR_TITLE), propertyManager.getPropertyValue(New_INPUT_ERROR_MESSAGE));
                        return;
                    }


//                    if (!alreadyGuessed(guess)) {
//                        savable = true;
//                        appTemplate.getGUI().updateWorkspaceToolbar(savable);
//                        boolean goodguess = false;
//                        for (int i = 0; i < progress.length; i++) {
//                            if (gamedata.getTargetWord().charAt(i) == guess) {
//                                progress[i].setVisible(true);
//                                gamedata.addGoodGuess(guess);
//                                goodguess = true;
//                                discovered++;
//
//                            }
//                        }
//                        if (!goodguess)
//                            gamedata.addBadGuess(guess);
//
//
//
//                        success = (discovered == progress.length);
//                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
//
//
//                    }
                });
//                if (gamedata.getRemainingGuesses() <= 0 || success)

                    stop();
            }

//            @Override
//            public void stop() {
//                super.stop();
//                end();
//                ArrayList<Character> list = new ArrayList<>(gamedata.getGoodGuesses());
//
//                for (int i = 0; i < gamedata.getTargetWord().length(); i++) {
//                    if(list.contains(gamedata.getTargetWord().charAt(i))){
//                        continue;
//                    }else {
////                        gameWorkspace.showNotGuessWord(i, gamedata.getTargetWord().charAt(i));
//                    }
//
//                }
//
//
//
//
//
//
//            }
        };
        timer.start();
    }

//    private boolean alreadyGuessed(char c) {
//        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
//    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
//        System.out.println(savable);

        if (savable)
            try {
                promptToSave();
                savable=!makenew;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file


            gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

//            gameWorkspace.reinitialize();
//            enableGameButton();


//            gameWorkspace.drawHangman(10);


        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (hintButton!=null){
            hintButton.setVisible(false);
        }

    }


    public void handlecreatNewRequest(){
        PropertyManager propertyManager = PropertyManager.getManager();


        try {
            promptToSave();
            gameWorkspace.levelSelection();

        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }

    }
    @Override
    public void handleSaveRequest() throws IOException {
    }

    public void handleSelectionPage(){

    }


    public void handleLoginRequest() throws IOException{


        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();


//        FileChooser fileChooser = new FileChooser();
        URL workDirURL = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/1.json").toURI().toURL();
        if (workDirURL == null)
            throw new FileNotFoundException("Work folder not found under resources.");
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json file", "*.json"));
        File initialDir1 = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/1.json");
        File initialDir2 = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/2.json");
        File initialDir3 = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/3.json");
        File initialDir4 = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/4.json");
        File initialDir5 = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata/5.json");

//        if (!initialDir.exists())
//            initialDir.mkdirs();
//        fileChooser.setInitialDirectory(initialDir);
//        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
//                propertyManager.getPropertyValue(WORK_FILE_EXT)));
//        File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());

        try {

            if (initialDir1.length() > 0) {


                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)

                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                workFile = null;                                       // new workspace has never been saved to a file

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gamedata = new GameData(appTemplate);

                appTemplate.getFileComponent().loadData(gamedata, initialDir1.toPath());
                    workFile = initialDir1.toPath();

                loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();

                    if(gamedata.getuserName().equals(dialog.getuserName()) ) {
                        if(gamedata.getPassWord().equals(dialog.getPassWord())) {


                            levelForWordspace = gamedata.getLevel();


                            LoginUserName = gamedata.getuserName();

                            gameWorkspace.getName().setText(LoginUserName);
                            gameWorkspace.levelSelection();
                        }
                    }
            }

            if (initialDir2.length() > 0) {


                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)

                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                workFile = null;                                       // new workspace has never been saved to a file

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gamedata = new GameData(appTemplate);
                appTemplate.getFileComponent().loadData(gamedata, initialDir2.toPath());
                workFile = initialDir2.toPath();

                loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();

                if(gamedata.getuserName().equals(dialog.getuserName()) ) {
                    if(gamedata.getPassWord().equals(dialog.getPassWord())) {
//                        gamedata.setuserName(gamedata.getuserName());
//
//                        gamedata.setpassWord(gamedata.getPassWord());
//
//                        gamedata.setLevel(gamedata.getLevel());

                        levelForWordspace = gamedata.getLevel();


                        LoginUserName = gamedata.getuserName();
                        gameWorkspace.getName().setText(LoginUserName);
                        gameWorkspace.levelSelection();
                    }
                }
            }

            if (initialDir3.length() > 0) {


                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)

                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                workFile = null;                                       // new workspace has never been saved to a file

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gamedata = new GameData(appTemplate);
                appTemplate.getFileComponent().loadData(gamedata, initialDir3.toPath());
                workFile = initialDir3.toPath();

                loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();

                if(gamedata.getuserName().equals(dialog.getuserName()) ) {
                    if(gamedata.getPassWord().equals(dialog.getPassWord())){
//                        gamedata.setuserName(gamedata.getuserName());
//
//                        gamedata.setpassWord(gamedata.getPassWord());
//
//                        gamedata.setLevel(gamedata.getLevel());


                        levelForWordspace = gamedata.getLevel();


                        LoginUserName = gamedata.getuserName();
                    gameWorkspace.getName().setText(LoginUserName);
                    gameWorkspace.levelSelection();
                    }
                }
            }

            if (initialDir4.length() > 0) {


                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)

                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                workFile = null;                                       // new workspace has never been saved to a file

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gamedata = new GameData(appTemplate);
                appTemplate.getFileComponent().loadData(gamedata, initialDir4.toPath());
                workFile = initialDir4.toPath();

                loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();

                if(gamedata.getuserName().equals(dialog.getuserName()) ) {
                    if(gamedata.getPassWord().equals(dialog.getPassWord())) {
//                        gamedata.setuserName(gamedata.getuserName());
//
//                        gamedata.setpassWord(gamedata.getPassWord());
//
//                        gamedata.setLevel(gamedata.getLevel());

                        levelForWordspace = gamedata.getLevel();


                        LoginUserName = gamedata.getuserName();
                        gameWorkspace.getName().setText(LoginUserName);
                        gameWorkspace.levelSelection();
                    }
                }
            }

            if (initialDir5.length() > 0) {


                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)

                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                workFile = null;                                       // new workspace has never been saved to a file

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gamedata = new GameData(appTemplate);
                appTemplate.getFileComponent().loadData(gamedata, initialDir5.toPath());
                workFile = initialDir5.toPath();

                loginDialogSingLeton dialog = loginDialogSingLeton.getSingleton();

                if(gamedata.getuserName().equals(dialog.getuserName()) ) {
                    if(gamedata.getPassWord().equals(dialog.getPassWord())) {


                        levelForWordspace = gamedata.getLevel();

                        LoginUserName = gamedata.getuserName();
                        gameWorkspace.getName().setText(LoginUserName);
                        gameWorkspace.levelSelection();
                    }
                }
            }

        }catch(IOException ioe){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        }

    }

    public String getLoginUserName(){
        return LoginUserName;
    }

    public String getLevelForWordspace(){
        return levelForWordspace;
    }
    @Override
    public void handleLoadRequest() throws IOException {
//
//
//        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
//        PropertyManager propertyManager = PropertyManager.getManager();
//        boolean makeload = true;
//
//
//        FileChooser fileChooser = new FileChooser();
//        URL workDirURL = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata").toURI().toURL();
//        if (workDirURL == null)
//            throw new FileNotFoundException("Work folder not found under resources.");
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json file", "*.json"));
//        File initialDir = new File(HangmanController.class.getClassLoader().getResource("").getFile(), "saveddata");
//        if (!initialDir.exists())
//            initialDir.mkdirs();
//        fileChooser.setInitialDirectory(initialDir);
//        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
//                propertyManager.getPropertyValue(WORK_FILE_EXT)));
//        File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());
//
//        try {
//
//            if (savable)
//                try {
//
//                    makeload = promptToSave();
//                    savable=false;
//                } catch (IOException e) {
//                    messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
//                }
//            if (selectedFile!=null) {
//
//
//                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
//
//                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
//                ensureActivatedWorkspace();                            // ensure workspace is activated
//                workFile = null;                                       // new workspace has never been saved to a file
//
//                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
//                gameWorkspace.reinitialize();
//                enableGameButton();
//                savable = true;
//                if (gameover) {
//                    savable = false;
//                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
//                    gameWorkspace.reinitialize();
//                    enableGameButton();
//                }
//                if (selectedFile == null) {
//                    savable = false;
//                }
//                else {
//                    gamedata = new GameData(appTemplate);
//                    appTemplate.getFileComponent().loadData(gamedata, selectedFile.toPath());
//                    workFile = selectedFile.toPath();
//
//                    gameover = false;
//                    success = false;
//                    savable = false;
//                    discovered = 0;
//                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
//                    HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
//                    HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
//
//                    remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
//                    remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
//                    initWordGraphics(guessedLetters);
//
//                    Object[] goodData = gamedata.getGoodGuesses().toArray();
//                    Set<Character> veryGoodData = new HashSet<>();
//                    Set<Character> veryBadData = new HashSet<>();
//
//                    Object[] badData = gamedata.getBadGuesses().toArray();
//
//                    for (int b = 0; b<badData.length;b++){
//                        veryBadData.add(badData[b].toString().charAt(0));
//                    }
//
//                    for (int j = 0; j < goodData.length; j++) {
//                        veryGoodData.add(goodData[j].toString().charAt(0));
//                        for (int a = 0; a < progress.length; a++) {
//                            if (gamedata.getTargetWord().charAt(a) == goodData[j].toString().charAt(0)) {
//                                progress[a].setVisible(true);
//                                discovered++;
//                            }
//                        }
//
//                    }
//                    gamedata.setGoodGuesses(veryGoodData);
//                    gamedata.setBadGuesses(veryBadData);
//                    disableGameButton();
//
//
//
//
//
//
//                    ArrayList<Character> list = new ArrayList<>(gamedata.getGoodGuesses());
//                    for (int n = 0; n < gamedata.getGoodGuesses().size();n++) {
//
//                        for (int i = 0; i < gamedata.getTargetWord().length(); i++) {
//
//                            if (list.get(n)==gamedata.getTargetWord().charAt(i)) {
//                            }
//                        }
//                    }
//
//
//                    if(gamedata.getBadGuesses().size()<10-gamedata.getRemainingGuesses()){
//                        hintButton.setDisable(true);
//                    }
//
//                    if (gamedata.getTargetWord().length()<7){
//                        hintButton.setVisible(false);
//                    }
//
//
//
//                    play();
//                }
//
//            }
//        }catch(IOException ioe){
//                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//                dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
//        }
    }



    @Override
    public void handleExitRequest() {

        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(QUIT_NOTQUIT_TITLE),
                propertyManager.getPropertyValue(QUIT_NOTQUIT_MESSAGE));

        try {
            if (yesNoCancelDialog.getSelection()!=null&&yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)){
                System.exit(0);
            }
        } catch (Exception ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private void promptToSave() throws IOException, NullPointerException {
        PropertyManager            propertyManager   = PropertyManager.getManager();

            if (workFile != null)
                save(workFile);
            else {
                FileChooser fileChooser = new FileChooser();

                URL         workDirURL  = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/1.json").toURI().toURL();
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

//                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json file","*.json"));

                File initialDir1 = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/1.json");
                File initialDir2 = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/2.json");
                File initialDir3 = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/3.json");
                File initialDir4 = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/4.json");
                File initialDir5 = new File(HangmanController.class.getClassLoader().getResource("").getFile(),"saveddata/5.json");

//                if (!initialDir.exists())
//                    initialDir.mkdirs();
//
//                fileChooser.setInitialDirectory(initialDir);
//                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
//                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
//                        propertyManager.getPropertyValue(WORK_FILE_EXT)));
//                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
//              if(initialDir.isDirectory()){
                if(initialDir1.length()==0){
                    save(initialDir1.toPath());
                }
                else if(initialDir2.length()==0){
                    save(initialDir2.toPath());
                }
                else if(initialDir3.length()==0){
                    save(initialDir3.toPath());
                }
                else if(initialDir4.length()==0){
                    save(initialDir4.toPath());
                }
                else if(initialDir5.length()==0){
                    save(initialDir5.toPath());
                }


            }
//        }
//        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);

    }

    private void Load(Path target) throws IOException {

        appTemplate.getFileComponent()
                .loadData(appTemplate.getDataComponent() , target);

        workFile=target;


    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

        appTemplate.getFileComponent()
                .saveData(appTemplate.getDataComponent() , target);

        workFile=target;

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));


    }

    public int computertarget(){
        return 1;
    }
}

