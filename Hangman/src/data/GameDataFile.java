package data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import components.AppDataComponent;
import components.AppFileComponent;
import jdk.nashorn.internal.parser.JSONParser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.util.HashMap;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;

import static settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static settings.AppPropertyType.PROPERTIES_LOAD_ERROR_TITLE;
//
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;




/**
 * @author Ritwik Banerjee , CHEN JIN
 */
public class GameDataFile implements AppFileComponent {

    public static final String USER_NAME  = "USER_NAME";
    public static final String PASS_WORD = "PASS_WORD";
    public static final String LEVEL_SELECT = "LEVEL_SELECT";



    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException{
        GameData gamedata = (GameData) data;

            HashMap<String, Object> hashmap = new HashMap<>();
            ObjectMapper mapper = new ObjectMapper();

            hashmap.put(USER_NAME, gamedata.getuserName());

            hashmap.put(PASS_WORD, gamedata.getPassWord());


            hashmap.put(LEVEL_SELECT, gamedata.getLevel());


            mapper.writeValue(new File(to.toUri()), hashmap);

        }


    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        ObjectMapper mapper = new ObjectMapper();

        GameData gamedata = (GameData) data;

        TypeReference<Map<String,Object>> ref = new TypeReference<Map<String,Object>>(){};
        Map<String, Object> map = mapper.readValue(from.toFile(),ref);

        try{
        gamedata.setuserName(map.get(USER_NAME).toString());
        gamedata.setpassWord(map.get(PASS_WORD).toString());
        gamedata.setLevel(map.get(LEVEL_SELECT).toString());
//        gamedata.setRemainingGusses(Integer.parseInt(map.get(REMAINING_GUESSES).toString()));
        }
        catch(Exception ioe){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));

        }





    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
