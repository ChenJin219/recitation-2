package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee, CHEN JIN
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         userName;
    private String         passWord;
//    private Set<Character> goodGuesses;
//    private Set<Character> badGuesses;
    private String            level;

//    @Override
//    public String toString() {
//        return "GameData{" +
//                "targetWord='" + targetWord + '\'' +
//                ", goodGuesses=" + goodGuesses +
//                ", badGuesses=" + badGuesses +
//                ", remainingGuesses=" + remainingGuesses +
//                ", appTemplate=" + appTemplate +
//                '}';
//    }

    public  AppTemplate    appTemplate;

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.appTemplate.setDataComponent(this);
        this.userName = null;
        this.passWord = null;
        this.level = null;
//        this.level = TOTAL_NUMBER_OF_GUESSES_ALLOWED;


    }

    @Override
    public void reset() {
        this.userName = null;
        this.passWord=null;
        this.level = null;
//        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getuserName() {
        return userName;
    }

    public String getPassWord(){return passWord;}


    public String getLevel(){return level;}
    public void setuserName(String userName ) {
        this.userName = userName;

    }

    public void setpassWord(String passWord){
        this.passWord = passWord;

       }

    public void setLevel(String level){
        this.level=level;
    }




//    public GameData setuserName(String userName) {
//        this.level = userName;
//
//        return this;
//    }

//    public Set<Character> getGoodGuesses() {
//        return goodGuesses;
//    }
//
//    @SuppressWarnings("unused")
//    public GameData setGoodGuesses(Set<Character> goodGuesses) {
//        this.goodGuesses = goodGuesses;
//        return this;
//    }
//
//    public Set<Character> getBadGuesses() {
//        return badGuesses;
//    }
//
//    @SuppressWarnings("unused")
//    public GameData setBadGuesses(Set<Character> badGuesses) {
//        this.badGuesses = badGuesses;
//        return this;
//    }
//    public GameData setRemainingGusses(int remainingGuesses){
//        this.remainingGuesses = remainingGuesses;
//        return this;
//
//    }
//
//    public int getRemainingGuesses() {
//        return remainingGuesses;
//    }

//    public void addGoodGuess(char c) {
//        goodGuesses.add(c);
//    }
//
//    public void addBadGuess(char c) {
//        if (!badGuesses.contains(c)) {
//            badGuesses.add(c);
//            remainingGuesses--;
//        }
//    }


}
